const mongoose = require("mongoose");

const UsuarioSchema = mongoose.Schema({
  nombre: {
    type: String,
    required: true,
    trim: true,
  },
  correo: {
    type: String,
    required: true,
    trim: true,
    unique: true, //solo un correo por usuario
    lowercase: true, //almacena todo en minuscula
  },
  password: {
    type: String,
    required: true,
    trim: true,
  },
  nivel: {
    type: Number,
    required: true,
    default: 0,
  },
  sucres: {
    type: Number,
    default: 200,
  },
  estado: {
    type: Boolean,
    default: true,
  },
  registro: {
    type: Date,
    default: Date.now(),
  },
  role: {
    type: String,
    default: "player",
  },
  telephone: {
    type: String,
    default: "none",
  },
  address: {
    type: String,
    default: "none",
  },
  DNI: {
    type: String,
    default: "none",
  },
});
module.exports = mongoose.model("Usuario", UsuarioSchema);
