const mongoose = require("mongoose");

const PersonajeSchema = mongoose.Schema({
  nombre: {
    type: String,
    required: true,
  },
  descripcion: {
    type: String,
    required: true,
  },

  costo: {
    type: Number,
    required: true,
    default: 0,
  },
  imagen: {
    type: String,
    required: true,
    trim: true,
    default: "",
  },
  
  objectTag: {
    type: String,
    required: true,
    default: "",
  },
  
});
module.exports = mongoose.model("Personaje", PersonajeSchema);
