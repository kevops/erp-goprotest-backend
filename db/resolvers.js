//importacion de Schemas
const Usuario = require("../models/Usuario");
const Compras = require("../models/Compras");
const Consumible = require("../models/Consumible");
const Personaje = require("../models/Personaje");
const Ropa = require("../models/Ropa");
const SkinsEspecial = require("../models/SkinsEspecial");
const UserConsumible = require("../models/UserConsumible");
const UserPersonaje = require("../models/UserPersonaje");
const UserRopa = require("../models/UserRopa");
const UserSkinsEspecial = require("../models/UserSkinsEspecial");
const CompraMonedas = require("../models/CompraMonedas");
const Cancion = require("../models/Cancion");
//Dependencias de seguridad
const bcryptjs = require("bcryptjs");
const jwt = require("jsonwebtoken");
require("dotenv").config({ path: "variables.env" });
//crea y firma un token
const crearToken = (usuario, secreta, expiresIn) => {
  const { id, correo, nombre } = usuario;
  return jwt.sign({ id, correo, nombre }, secreta, { expiresIn });
};
//implementacion de servicio de correo
//const inbox = require('../mailService/mail');
const sendMailToUser = require("../mailService/sendMail");
const generatePasswordRand = require("../mailService/generatePassword");

const consumibles = [
  {
    id: "3",
    nombre: "Molotov",
    tipo: "arma",
    descripcion: "Bombas para manifestantes",
    dano: 60,
    costo: 350,
    puntosVida: null,
    disponibilidad: true,
  },
  {
    id: "2",
    nombre: "Lagrimogena",
    tipo: "arma",
    descripcion: "Bombas lagrimógenas para policias",
    dano: 40,
    costo: 300,
    puntosVida: null,
    disponibilidad: true,
  },
  {
    id: "1",
    nombre: "Bala",
    tipo: "arma",
    descripcion: "Balas reales para policias",
    dano: 40,
    costo: 100,
    puntosVida: null,
    disponibilidad: true,
  },
];

const resolvers = {
  Query: {
    obtenerCanciones: async (_, { input }, ctx) => {
      const canciones = await Cancion.find();
      return canciones;
    },
    obtenerConsumibles: async (_, { input }, ctx) => {
      const userConsumibles = await Consumible.find();
      return userConsumibles;
    },
    obtenerArmas: async (_, { input }, ctx) => {
      const armas = await Consumible.find().where("tipo").equals("Arma");
      return armas;
    },
    obtenerInsumos: async (_, { input }, ctx) => {
      const insumos = await Consumible.find().where("tipo").equals("Insumos");
      return insumos;
    },
    //obtener personajes todos
    obtenerPersonajes: async (_, { input }, ctx) => {
      const personajes = await Personaje.find();
      return personajes;
    },

    //obtener skins especiales todos
    obtenerSkins: async (_, { input }, ctx) => {
      const skins = await SkinsEspecial.find();
      return skins;
    },
    //obtener ropa todos
    obtenerRopa: async (_, { input }, ctx) => {
      const ropa = await Ropa.find();
      return ropa;
    },
    //obtener usuarios todos
    obtenerUsuarios: async (_, { input }, ctx) => {
      const usuarios = await Usuario.find();
      return usuarios;
    },
    //obtener todas las compras
    obtenerCompras: async (_, { input }, ctx) => {
      const compras = await Compras.find();
      return compras;
    },
    //obtener compras segun el ID del usuario
    obtenerComprasID: async (_, { id, input }, ctx) => {
      const comprasID = await Compras.find({ idUsuario: id });
      return comprasID;
    },
    //obtener todas las compras de monedas
    obtenerComprasMoneda: async (_, { input }, ctx) => {
      const comprasMonedas = await CompraMonedas.find();
      return comprasMonedas;
    },
    //obtener compras de monedas segun el ID del usuario
    obtenerComprasMonedaID: async (_, { input }, ctx) => {
      const comprasMonedasID = await CompraMonedas.find({ idUsuario: id });
      return comprasMonedasID;
    },
    obtenerUserConsumibles: async (_, { input }, ctx) => {
      const userConsumibles = await UserConsumible.find({
        idUsuario: ctx.usuario.id,
      });
      return userConsumibles;
    },
    obtenerUserPersonajes: async (_, { input }, ctx) => {
      const userConsumibles = await UserPersonaje.find({
        idUsuario: ctx.usuario.id,
      });
      return userConsumibles;
    },
    obtenerUserRopa: async (_, { input }, ctx) => {
      const userConsumibles = await UserRopa.find({
        idUsuario: ctx.usuario.id,
      });
      return userConsumibles;
    },
    obtenerUserSkinsEspeciales: async (_, { input }, ctx) => {
      const userConsumibles = await UserSkinsEspecial.find({
        idUsuario: input.idUser,
      });
      return userConsumibles;
    },
  },
  Mutation: {
    //CRUD USUARIOS
    crearUsuario: async (_, { input }) => {
      const { correo, password } = input;
      const existeUsuario = await Usuario.findOne({ correo });
      console.log(existeUsuario);
      //si existe usuario
      if (existeUsuario) {
        throw new Error("El usuario ya existe");
      }
      try {
        //hashear password
        const salt = await bcryptjs.genSalt(10);
        input.password = await bcryptjs.hash(password, salt);
        const nuevoUsuario = new Usuario(input);
        nuevoUsuario.save();
        return "Usuario creado con éxito!";
      } catch (error) {
        console.log(error);
      }
    },
    autenticarUsuario: async (_, { input }) => {
      const { nombre, correo, password } = input;
      //verificar que el usuario exista
      const existeUsuario = await Usuario.findOne({ correo });
      if (!existeUsuario) {
        throw new Error("El usuario no está registrado!");
      }
      //si el password es correcto
      const passwordCorrecto = await bcryptjs.compare(
        password,
        existeUsuario.password
      ); //compara el password con el otro hasheado
      console.log(passwordCorrecto); //para verificar si el password esta bien escrito
      if (!passwordCorrecto) {
        throw new Error("Password incorrecto!");
      }

      return {
        token: crearToken(existeUsuario, process.env.SECRETA, "2hr"),
        nombre: existeUsuario.nombre,
        correo: existeUsuario.correo,
        userID: existeUsuario.id,
        role: existeUsuario.role,
      };
    },
    //existeUsuario: existeUsuario,
    editarUsuario: async (_, { id, input }, ctx) => {
      let usuario = await Usuario.findById(id);

      console.log("el usuario existe!");
      console.log("password", input.password);
      //verificar si hay el usuario
      if (!usuario) {
        throw new Error("Usuario no encontrado!");
      }
      //verificacion de cambio en Password
      //si el password es correcto
      if (typeof input.password === "undefined") {
        console.log("no cambia password");
        usuario = await Usuario.findOneAndUpdate({ _id: id }, input);
        return await usuario;
      } else {
        console.log("entro a cambiar password");
        const { password } = input;
        //hashear password, asi no haya cambiado
        const salt = await bcryptjs.genSalt(10);
        input.password = await bcryptjs.hash(password, salt);
        //guardar y retornar el usuario
        usuario = await Usuario.findOneAndUpdate({ _id: id }, input);
        return await usuario;
      }
    },
    encontrarUsuarioPorEmail: async (_, { input }, ctx) => {
      const { correo } = input;
      let usuario = await Usuario.findOne({ correo });
      if (!usuario) {
        throw new Error("Usuario no encontrado!");
      } else {
        console.log("El usuario existe!");
        console.log("Este es el usuario", usuario);
      }
      return usuario;
    },
    encontrarUsuarioPorDNI: async (_, { input }, ctx) => {
      const { DNI } = input;
      let usuario = await Usuario.findOne({ DNI });
      if (!usuario) {
        throw new Error("Usuario no encontrado!");
      } else {
        console.log("El usuario existe!");
        console.log("Este es el usuario", usuario);
      }
      return usuario;
    },
    encontrarConsumibleByObjectTag: async (_, { input }, ctx) => {
      
      const { objectTag, tipo } = input;
      if (tipo === "character") {
        let character = await Personaje.findOne({ objectTag });
        if (!character) {
          throw new Error("Character not found!");
        } else {
          console.log("Character found!");
          console.log("This is the Character", character);
          return {Personaje: character};
        }
      } else if (tipo === "clothing") {
        let clothing = await Ropa.findOne({ objectTag });
        if (!clothing) {
          throw new Error("Clothing not found!");
        } else {
          console.log("Clothing found!");
          console.log("This is the Clothing", clothing);
          return {Ropa:clothing};
        }
      } else if (tipo === "skin") {
        let skin = await SkinsEspecial.findOne({ objectTag });
        if (!skin) {
          throw new Error("Skin not found!");
        } else {
          console.log("Skin found!");
          console.log("This is the Skin", skin);
          return {Skin: skin};
        }
      } 
      else if (tipo === "weapon" || tipo === "munition" || tipo === "food" || tipo === "health"  ) {
        let consumible = await Consumible.findOne({ objectTag });
        if (!consumible) {
          throw new Error("Consumible not found!");
        } else {
          console.log("Consumible found!");
          console.log("This is the Consumible", consumible);
          return {Consumible: consumible};
        }
      }  else {
        throw new Error("Type not found!");
      }
    },
    eliminarUsuario: async (_, { id }, ctx) => {
      let usuario = await Usuario.findById(id);
      if (!usuario) {
        throw new Error("Usuario no encontrado!");
      }

      await Usuario.findOneAndDelete({ _id: id });
      return "Usuario Eliminado";
    },

    recuperarContrasenia: async (_, { input }, ctx) => {
      //users.find(user => user.id === args.id);
      console.log("input correo", input.correo);
      let usuario = await Usuario.findOne({ correo: input.correo });
      console.log("Usuario encontrado", usuario);

      //hashear password, asi no haya cambiado
      const salt = await bcryptjs.genSalt(10);
      const newPassword = generatePasswordRand(10, "rand");
      usuario.password = await bcryptjs.hash(newPassword, salt);
      console.log("password nuevo", usuario.password);
      //guardar y retornar el usuario
      usuario = await Usuario.findOneAndUpdate({ _id: usuario.id }, usuario);
      sendMailToUser(
        "9a931750-e804-4c1a-828f-7eeb9060b46b",
        newPassword,
        input.correo
      );
      return "Clave cambiada con exito!";
    },
    //CRUD CONSUMIBLES
    nuevoConsumible: async (_, { input }, ctx) => {
      try {
        const consumible = new Consumible(input);

        //almacenar en la base de datos
        const resultado = await consumible.save();
        return resultado;
      } catch (error) {
        console.log(error);
      }
    },
    actualizarConsumible: async (_, { id, input }, ctx) => {
      let consumible = await Consumible.findById(id);
      if (!consumible) {
        throw new Error("Consumible no encontrado!");
      }

      //guardar y retornar la tarea
      consumible = await Consumible.findOneAndUpdate({ _id: id }, input);
      return consumible;
    },
    eliminarConsumible: async (_, { id }, ctx) => {
      let consumible = await Consumible.findById(id);
      if (!consumible) {
        throw new Error("Consumible no encontrado!");
      }
      //eliminar consumible
      await Consumible.findOneAndDelete({ _id: id });
      return "Consumible Eliminado";
    },
    //CRUD PERSONAJES
    nuevoPersonaje: async (_, { input }, ctx) => {
      try {
        const personaje = new Personaje(input);
        //almacenar en la base de datos
        const resultado = await personaje.save();
        return resultado;
      } catch (error) {
        console.log(error);
      }
    },
    actualizarPersonaje: async (_, { id, input }, ctx) => {
      let personaje = await Personaje.findById(id);
      if (!personaje) {
        throw new Error("Personaje no encontrado!");
      }

      //guardar y retornar la tarea
      personaje = await Personaje.findOneAndUpdate({ _id: id }, input);
      return personaje;
    },
    eliminarPersonaje: async (_, { id }, ctx) => {
      let personaje = await Personaje.findById(id);
      if (!personaje) {
        throw new Error("Personaje no encontrado!");
      }
      //eliminar consumible
      await Personaje.findOneAndDelete({ _id: id });
      return "Personaje Eliminado";
    },
    //CRUD Ropa
    nuevaRopa: async (_, { input }, ctx) => {
      try {
        const ropa = new Ropa(input);
        //almacenar en la base de datos
        const resultado = await ropa.save();
        return resultado;
      } catch (error) {
        console.log(error);
      }
    },
    actualizarRopa: async (_, { id, input }, ctx) => {
      let ropa = await Ropa.findById(id);
      if (!ropa) {
        throw new Error("Ropa no encontrada!");
      }

      //guardar y retornar la tarea
      ropa = await Ropa.findOneAndUpdate({ _id: id }, input);
      return ropa;
    },
    eliminarRopa: async (_, { id }, ctx) => {
      let ropa = await Ropa.findById(id);
      if (!ropa) {
        throw new Error("Ropa no encontrada!");
      }
      //eliminar consumible
      await Ropa.findOneAndDelete({ _id: id });
      return "Personaje Eliminado";
    },
    //CRUD SKIN ESPECIAL
    nuevaSkin: async (_, { input }, ctx) => {
      try {
        const skin = new SkinsEspecial(input);
        const resultado = await skin.save();
        return resultado;
      } catch (error) {
        console.log(error);
      }
    },
    actualizarSkin: async (_, { id, input }, ctx) => {
      let skin = await SkinsEspecial.findById(id);
      if (!skin) {
        throw new Error("Skin no encontrada!");
      }

      //guardar y retornar la tarea
      skin = await SkinsEspecial.findOneAndUpdate({ _id: id }, input);
      return skin;
    },
    eliminarSkin: async (_, { id }, ctx) => {
      let skin = await SkinsEspecial.findById(id);
      if (!skin) {
        throw new Error("Skin no encontrada!");
      }
      //eliminar consumible
      await SkinsEspecial.findOneAndDelete({ _id: id });
      return "Skin Especial Eliminada";
    },
    //CRUD COMPRAR MONEDAS
    nuevaCompraMonedas: async (_, { input }, ctx) => {
      try {
        const compraMoneda = new CompraMonedas(input);
        //asociar el creador del proyecto
        compraMoneda.IDUsuario = ctx.usuario.id;
        //almacenar en la base de datos
        const resultado = await compraMoneda.save();
        return resultado;
      } catch (error) {
        console.log(error);
      }
    },
    actualizarCompraMonedas: async (_, { id, input }, ctx) => {
      //EXISTE EL PROYECTO?
      let compraMoneda = await CompraMonedas.findById(id);
      if (!compraMoneda) {
        throw new Error("Compra no encontrada!");
      }
      //LA PERSONA EDITORA ES EL CREADOR?
      if (compraMoneda.IDUsuario.toString() !== ctx.usuario.id) {
        throw new Error("No tiene las credenciales para editar compra");
      }
      //gaurdar proyecto
      compraMoneda = await compraMoneda.findOneAndUpdate({ _id: id }, input, {
        new: true,
      });
      return compraMoneda;
    },
    cancelarCompraMonedas: async (_, { id, input }, ctx) => {
      //EXISTE EL PROYECTO?
      let compraMoneda = await CompraMonedas.findById(id);
      if (!compraMoneda) {
        throw new Error("Proyecto no encontrado!");
      }
      //LA PERSONA EDITORA ES EL CREADOR?
      if (compraMoneda.IDUsuario.toString() !== ctx.usuario.id) {
        throw new Error("No tiene las credenciales para eliminar");
      }
      //eliminar
      await compraMoneda.findOneAndDelete({ _id: id });
      return "Compra de Moneda Eliminada";
    },
    //CRUD COMPRAS
    nuevaCompra: async (_, { input }, ctx) => {
      try {
        const compra = new Compras(input);
        //asociar el creador del proyecto
        compra.IDUsuario = ctx.usuario.id;
        //almacenar en la base de datos
        const resultado = await compra.save();
        return resultado;
      } catch (error) {
        console.log(error);
      }
    },
    actualizarCompra: async (_, { id, input }, ctx) => {
      //EXISTE EL PROYECTO?
      let compra = await Compras.findById(id);
      if (!compra) {
        throw new Error("Compra no encontrada!");
      }
      //LA PERSONA EDITORA ES EL CREADOR?
      if (compraMoneda.IDUsuario.toString() !== ctx.usuario.id) {
        throw new Error("No tiene las credenciales para editar compra");
      }
      //gaurdar proyecto
      compra = await compra.findOneAndUpdate({ _id: id }, input, { new: true });
      return compra;
    },
    cancelarCompra: async (_, { id, input }, ctx) => {
      //EXISTE EL PROYECTO?
      let compra = await Compras.findById(id);
      if (!compra) {
        throw new Error("Compra no encontrado!");
      }
      //LA PERSONA EDITORA ES EL CREADOR?
      if (compra.IDUsuario.toString() !== ctx.usuario.id) {
        throw new Error("No tiene las credenciales para eliminar");
      }
      //eliminar
      await compra.findOneAndDelete({ _id: id });
      return "Compra Eliminada";
    },
    //CRUD User Consumible
    nuevaUserConsumible: async (_, { input }, ctx) => {
      try {
        const userConsumible = new UserConsumible(input);
        //asociar el creador del proyecto
        //userConsumible.idUsuario = ctx.usuario.id;
        //almacenar en la base de datos
        const resultado = await userConsumible.save();
        return resultado;
      } catch (error) {
        console.log(error);
      }
    },
    actualizarUserConsumible: async (_, { id, input }, ctx) => {
      //EXISTE EL PROYECTO?
      let userConsumible = await UserConsumible.findById(id);
      if (!userConsumible) {
        throw new Error("Consumible no encontrada!");
      }
      //LA PERSONA EDITORA ES EL CREADOR?
      if (userConsumible.idUsuario.toString() !== ctx.usuario.id) {
        throw new Error("No tiene las credenciales para editar consumible");
      }

      //eliminar objeto si tiene 0 en dispiniblidad
      if (input.cantidadDisponible === 0) {
        await UserConsumible.findOneAndDelete({ _id: id });
        return "Compra Eliminada";
      } else {
        //guardar proyecto
        userConsumible = await UserConsumible.findOneAndUpdate(
          { _id: id },
          input,
          { new: true }
        );
        return userConsumible;
      }
    },
    cancelarUserConsumible: async (_, { id, input }, ctx) => {
      //EXISTE EL PROYECTO?
      let userConsumible = await UserConsumible.findById(id);
      if (!userConsumible) {
        throw new Error("Consumible no encontrado!");
      }
      //LA PERSONA EDITORA ES EL CREADOR?
      if (userConsumible.idUsuario.toString() !== ctx.usuario.id) {
        throw new Error("No tiene las credenciales para eliminar");
      }
      //eliminar
      await userConsumible.findOneAndDelete({ _id: id });
      return "Compra Eliminada";
    },
    //CRUD User Personaje
    nuevoUserPersonaje: async (_, { input }, ctx) => {
      try {
        const userPersonaje = new UserPersonaje(input);
        //asociar el creador del proyecto
        userPersonaje.idUsuario = ctx.usuario.id;
        //almacenar en la base de datos
        const resultado = await userPersonaje.save();
        return resultado;
      } catch (error) {
        console.log(error);
      }
    },
    actualizarUserPersonaje: async (_, { id, input }, ctx) => {
      //EXISTE EL PROYECTO?
      let userPersonaje = await UserPersonaje.findById(id);
      if (!userPersonaje) {
        throw new Error("Personaje no encontrado!");
      }
      //LA PERSONA EDITORA ES EL CREADOR?
      if (userPersonaje.idUsuario.toString() !== ctx.usuario.id) {
        throw new Error("No tiene las credenciales para editar consumible");
      }
      //guardar proyecto
      userPersonaje = await UserPersonaje.findOneAndUpdate({ _id: id }, input, {
        new: true,
      });
      return userPersonaje;
    },
    cancelarUserPersonaje: async (_, { id, input }, ctx) => {
      //EXISTE EL PROYECTO?
      let userPersonaje = await UserPersonaje.findById(id);
      if (!userPersonaje) {
        throw new Error("Consumible no encontrado!");
      }
      //LA PERSONA EDITORA ES LA DUENIA?
      if (userPersonaje.idUsuario.toString() !== ctx.usuario.id) {
        throw new Error("No tiene las credenciales para eliminar");
      }
      //eliminar
      await UserPersonaje.findOneAndDelete({ _id: id });
      return "Compra Eliminada";
    },
    //CRUD User Ropa
    nuevoUserRopa: async (_, { input }, ctx) => {
      try {
        const userRopa = new UserRopa(input);
        //asociar el creador del proyecto
        userRopa.idUsuario = ctx.usuario.id;
        //almacenar en la base de datos
        const resultado = await userRopa.save();
        return resultado;
      } catch (error) {
        console.log(error);
      }
    },
    actualizarUserRopa: async (_, { id, input }, ctx) => {
      //EXISTE EL PROYECTO?
      let userRopa = await UserRopa.findById(id);
      if (!userRopa) {
        throw new Error("Ropa no encontrada!");
      }
      //LA PERSONA EDITORA ES EL CREADOR?
      if (userRopa.idUsuario.toString() !== ctx.usuario.id) {
        throw new Error("No tiene las credenciales para editar consumible");
      }
      //guardar proyecto
      userRopa = await UserRopa.findOneAndUpdate({ _id: id }, input, {
        new: true,
      });
      return userRopa;
    },
    cancelarUserRopa: async (_, { id, input }, ctx) => {
      //EXISTE EL PROYECTO?
      let userRopa = await UserRopa.findById(id);
      if (!userRopa) {
        throw new Error("Consumible no encontrado!");
      }
      //LA PERSONA EDITORA ES LA DUENIA?
      if (userRopa.idUsuario.toString() !== ctx.usuario.id) {
        throw new Error("No tiene las credenciales para eliminar");
      }
      //eliminar
      await UserRopa.findOneAndDelete({ _id: id });
      return "Ropa Eliminada";
    },
    //CRUD User SKins
    nuevaUserSkinEspecial: async (_, { input }, ctx) => {
      try {
        const userSkinsEspecial = new UserSkinsEspecial(input);
        //asociar el creador del proyecto
        //userSkinsEspecial.idUsuario = input.idUsuario;
        //almacenar en la base de datos
        const resultado = await userSkinsEspecial.save();
        return resultado;
      } catch (error) {
        console.log(error);
      }
    },
    actualizarUserSkinEspecial: async (_, { id, input }, ctx) => {
      //EXISTE EL PROYECTO?
      let userSkinsEspecial = await UserSkinsEspecial.findById(id);
      if (!userSkinsEspecial) {
        throw new Error("Consumible no encontrada!");
      }
      //LA PERSONA EDITORA ES EL CREADOR?
      if (userSkinsEspecial.idUsuario.toString() !== ctx.usuario.id) {
        throw new Error("No tiene las credenciales para editar consumible");
      }

      //eliminar objeto si tiene 0 en dispiniblidad
      if (input.cantidadDisponible === 0) {
        await UserSkinsEspecial.findOneAndDelete({ _id: id });
        return "Compra Eliminada";
      } else {
        //guardar proyecto
        userSkinsEspecial = await UserSkinsEspecial.findOneAndUpdate(
          { _id: id },
          input,
          { new: true }
        );
        return userConsumible;
      }
    },
    cancelarUserSkinEspecial: async (_, { id, input }, ctx) => {
      //EXISTE EL PROYECTO?
      let userSkinsEspecial = await UserSkinsEspecial.findById(id);
      if (!userSkinsEspecial) {
        throw new Error("User Skins Especial no encontrado!");
      }
      //LA PERSONA EDITORA ES EL CREADOR?
      if (userSkinsEspecial.idUsuario.toString() !== ctx.usuario.id) {
        throw new Error("No tiene las credenciales para eliminar");
      }
      //eliminar
      await UserSkinsEspecial.findOneAndDelete({ _id: id });
      return "Skins Especial Eliminada";
    },
    //CRUD Canciones
    nuevaCancion: async (_, { input }, ctx) => {
      try {
        const cancion = new Cancion(input);
        //almacenar en la base de datos
        const resultado = await cancion.save();
        return resultado;
      } catch (error) {
        console.log(error);
      }
    },
    actualizarCancion: async (_, { id, input }, ctx) => {
      //EXISTE EL PROYECTO?
      let cancion = await Cancion.findById(id);
      if (!cancion) {
        throw new Error("Cancion no encontrada!");
      } else {
        //guardar proyecto
        cancion = await Cancion.findOneAndUpdate({ _id: id }, input, {
          new: true,
        });
        return cancion;
      }
    },
    eliminarCancion: async (_, { id, input }, ctx) => {
      //EXISTE EL PROYECTO?
      let cancion = await Cancion.findById(id);
      if (!cancion) {
        throw new Error("Consumible no encontrado!");
      }
      //eliminar
      await Cancion.findOneAndDelete({ _id: id });
      return "Canción Eliminada";
    },
  },
};

module.exports = resolvers;
