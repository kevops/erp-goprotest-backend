const { gql} = require('apollo-server');

const typeDefs =gql`
#TYPES
type Usuario{
  nombre: String
  correo: String
  password: String
  nivel:Int
  sucres: Int
  estado: Boolean
  registro: String
  role: String
  telephone: String
  address: String
  DNI: String
  id: ID
}
type Consumible {
    id:ID!
    nombre: String!
    tipo: String!
    descripcion: String!
    costo: Int
    imagen: String!
    dano: Int
    puntosDeVida: Int
    cantidad: Int
    danoPorSegundo: Int
    incrementoAliados: Int
    tiempoLiberacion: Int
    objectTag: String!
    estado: Boolean
    
}
type Personaje {
  id:ID!
  nombre: String!
  descripcion: String!
  costo: Int
  imagen: String!
  objectTag: String!
  
}

type Ropa {
  id:ID!
  nombre: String!
  descripcion: String!
  costo: Int
  imagen: String!
  objectTag: String!
}
type Skin {
  id:ID!
  nombreSkin: String!
  descripcion: String!
  costo: Int
  imagenSkin: String!
  objectTag: String!
  danoEspecial: Int
  puntosDeVidaExtra: Int
  disponibilidadTiempo: Int
  consumiblesIncluidos:[String]
  
}

type Token {
    token: String
    nombre: String
    correo: String
    nivel:Int
    sucres: Int
    estado: Boolean
    registro: String
    userID: String
    role: String
  }
 

type UsuarioEditado {
  nombre: String
  correo: String
  password: String
}
type CompraMoneda{
  nombreArticulo: String!
  cantidadMonedas: Int!
  fechaCompra: String
  totalCancelado: Float!
  IDUsuario: ID!
}
type Compra{
  nombreArticulo: String!
  IDConsumible: ID
  IDRopa: ID
  IDSkinEspecial: ID
  IDPersonaje: ID
  fechaCompra: String
  costo: Float!
  IDUsuario: ID
}
type UserConsumible{
  nombreConsumible: String!
  imagenConsumible: String!
  cantidadDisponible: Int!
  objectTag: String!
  idUsuario:ID!
  idConsumible:ID!
}
type UserPersonaje{
  nombrePersonaje: String
  imagenPersonaje: String
  objectTag: String
  idUsuario: ID
  idPersonaje: ID
}
type UserRopa{
  nombreRopa: String!
  imagenRopa: String!
  objectTag: String!
  idUsuario: ID!
  idRopa: ID!
}
type UserSkinEspecial{
  nombreSkin: String
  imagenSkin: String
  objectTag: String
  idUsuario: ID
  idSkinEspecial: ID
  cantidadDisponible: Int
}
type Cancion {
    nombre: String
    descripcion: String
    urlCancion: String
    lyrics: String
    autor: String
    objectTag: String
}
type GenericObject {
  Skin: Skin
  Ropa: Ropa
  Personaje: Personaje
  Consumible: Consumible

}


#INPUTS

input UsuarioInput {
    nombre: String!
    correo: String!
    password: String!
    role: String
    telephone: String
    address: String
    DNI: String
    nivel: Int
    sucres: Int
  }
  input AutenticarInput {
    correo: String!
    password: String!
    role: String!
  }
  input ConsumibleInput {
    nombre: String!
    tipo: String!
    descripcion: String!
    costo: Int
    imagen: String!
    dano: Int
    puntosDeVida: Int
    cantidad: Int
    danoPorSegundo: Int
    incrementoAliados: Int
    tiempoLiberacion: Int
    objectTag: String!
  }
  input PersonajeInput {
    nombre: String!
    descripcion: String!
    costo: Int
    imagen: String!
    objectTag: String!
  }
  input RopaInput {
    nombre: String!
    descripcion: String!
    costo: Int
    imagen: String!
    objectTag: String!
  }
  input SkinInput {
    nombreSkin: String!
    descripcion: String!
    costo: Int
    imagenSkin: String!
    objectTag: String!
    danoEspecial: Int
    puntosDeVidaExtra: Int
    disponibilidadTiempo: Int
    consumiblesIncluidos:[String]
    estado: Boolean
    
  }
  input UsuarioEditarInput{
    nombre: String
    correo: String
    password: String
    role: String
    telephone: String
    address: String
    DNI: String
    nivel: Int
    sucres: Int
    estado: Boolean

  }
  input CompraMonedaInput {
    cantidadMonedas: Int!
    totalCancelado: Float!
    IDUsuario: ID
  }
  input CompraInput{
    nombreArticulo: String!
    IDConsumible: ID
    IDRopa: ID
    IDSkinEspecial: ID
    IDPersonaje: ID
    costo: Float!
    
  }
  input UserConsumibleInput {
    nombreConsumible: String!
    imagenConsumible: String!
    cantidadDisponible: Int!
    objectTag: String!
    idUsuario:ID!
    idConsumible:ID!

  }
  input UserPersonajeInput{
    nombrePersonaje: String!
    imagenPersonaje: String!
    objectTag: String!
    idUsuario: ID!
    idPersonaje: ID!
  }
  input UserRopaInput{
    nombreRopa: String!
    imagenRopa: String!
    objectTag: String!
    idUsuario: ID!
    idRopa: ID!
  }
  input UserSkinEspecialInput {
    nombreSkin: String!
    imagenSkin: String!
    objectTag: String!
    idUsuario: ID!
    idSkinEspecial: ID!
    cantidadDisponible: Int!
    estado: Boolean
  }
  input UserIDInput {
    idUser: String!
  }
  input UserEmailInput{
    correo: String!
  }
  input UserDNIInput{
    DNI: String!
  }
  input CancionInput {
    nombre: String!
    descripcion: String!
    urlCancion: String!
    lyrics: String!
    autor: String!
    objectTag: String!
  }
  input CorreoInput {
    correo:String!
  }
  input ObjectTagInput {
    objectTag: String!
    tipo: String!
  }
  



type Mutation {
  #USUARIOS REGISTRO Y AUTENTICACION
    crearUsuario(input: UsuarioInput): String
    autenticarUsuario(input: AutenticarInput): Token
    editarUsuario(id: ID!, input: UsuarioEditarInput): UsuarioEditado
    eliminarUsuario(id: ID!): String
  #CONSUMIBLES CRUD
    nuevoConsumible(input: ConsumibleInput): Consumible
    actualizarConsumible(id: ID!, input: ConsumibleInput): Consumible
    eliminarConsumible(id: ID!): String
  #PERSONAJES CRD
    nuevoPersonaje(input: PersonajeInput): Personaje
    actualizarPersonaje(id: ID!, input: PersonajeInput): Personaje
    eliminarPersonaje(id: ID!): String
  #ROPA CRUD
    nuevaRopa(input: RopaInput): Ropa
    actualizarRopa(id: ID!, input: RopaInput): Ropa
    eliminarRopa(id: ID!): String
  #SKIN ESPECIALES CRUD
    nuevaSkin(input: SkinInput): Skin
    actualizarSkin(id: ID!, input: SkinInput): Skin
    eliminarSkin(id: ID!): String
  #COMPRAMONEDAS CRUD
    nuevaCompraMonedas(input: CompraMonedaInput): CompraMoneda
    actualizarCompraMonedas(id: ID!, input: CompraMonedaInput): CompraMoneda
    cancelarCompraMonedas(id: ID!): String
  #COMPRAS CRUD
    nuevaCompra(input: CompraInput): Compra
    actualizarCompra(id: ID!, input: CompraInput): Compra
    cancelarCompra(id: ID!): String
  #USERCONSUMIBLE CRUD
    nuevaUserConsumible(input: UserConsumibleInput): UserConsumible
    actualizarUserConsumible(id: ID!, input: UserConsumibleInput): UserConsumible
    cancelarUserConsumible(id: ID!): String
  #USERPERSONAJE CRUD
    nuevoUserPersonaje(input: UserPersonajeInput): UserPersonaje
    actualizarUserPersonaje(id: ID!, input: UserPersonajeInput): UserPersonaje
    cancelarUserPersonaje(id: ID!): String
  #USERROPA CRUD
    nuevoUserRopa(input: UserRopaInput): UserRopa
    actualizarUserRopa(id: ID!, input: UserRopaInput): UserRopa
    cancelarUserRopa(id: ID!): String
  #USERSKINESPECIALES CRUD
    nuevaUserSkinEspecial(input: UserSkinEspecialInput): UserSkinEspecial
    actualizarUserSkinEspecial(id: ID!, input: UserSkinEspecialInput): UserSkinEspecial
    cancelarUserSkinEspecial(id: ID!): String
  #CANCIONES CRUD
  nuevaCancion(input: CancionInput): Cancion
  actualizarCancion(id: ID!, input: CancionInput): Cancion
  eliminarCancion(id: ID!): String
  #RECUPERAR CONTRASEÑA
  recuperarContrasenia(input: CorreoInput): String
  #Encontrar usuario por ID
  encontrarUsuarioPorEmail(input: UserEmailInput): Usuario
  encontrarUsuarioPorDNI(input: UserDNIInput): Usuario
  encontrarConsumibleByObjectTag(input: ObjectTagInput): GenericObject
}
type Query {
  obtenerConsumibles: [Consumible]
  obtenerCanciones: [Cancion]
  obtenerArmas: [Consumible]
  obtenerInsumos: [Consumible]
  obtenerUserConsumibles(input: UserIDInput): [UserConsumible]
  obtenerUserPersonajes(input: UserIDInput): [UserPersonaje]
  obtenerUserRopa(input: UserIDInput): [UserPersonaje]
  obtenerUserSkinsEspeciales(input: UserIDInput): [UserSkinEspecial]
  #IMPLEMENTACIONES 16/07/2021
  obtenerPersonajes: [Personaje]
  obtenerSkins: [Skin]
  obtenerRopa: [Ropa]
  obtenerUsuarios: [Usuario]
  obtenerCompras: [Compra]
  obtenerComprasID(id: ID!):[Compra]
  obtenerComprasMoneda: [CompraMoneda]
  obtenerComprasMonedaID: [CompraMoneda]
  

  

}

`;
module.exports= typeDefs;