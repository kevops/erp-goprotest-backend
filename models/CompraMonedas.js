const mongoose = require('mongoose');
const CompraMonedasSchema = new mongoose.Schema({

    nombreArticulo: {
        type: String,
        require: true,
        trim: true,
        default: 'Sucres'
    },
    cantidadMonedas: {
        type: Number,
        required: true,
        default: 0,
      },
    
    fechaCompra: {
        type: String,
        default: Date.now()
    },
    totalCancelado: {
        type: Number,
        required: true,
        default: 0,
      },
    
    IDUsuario:{
        type: mongoose.Schema.Types.ObjectId,
        ref:'Usuario',
        require: true,
    },

})
module.exports = mongoose.model('CompraMonedas', CompraMonedasSchema);