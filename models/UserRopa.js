const mongoose = require('mongoose');

const UserRopaSchema = mongoose.Schema(
    {
        nombreRopa:{
            type: String,
            required: true,
            trim: true,
        },
        imagenRopa:{
            type: String,
            required: true,
            trim: true,
        },
    
        objectTag:{
            type: String,
            required: true,
            trim: true,
        },
        
        idUsuario: {
            type: mongoose.Schema.Types.ObjectId,
            ref:'Usuario'
        },
        idRopa: {
            type: mongoose.Schema.Types.ObjectId,
            ref:'Ropa'
        },
       
    }
);
module.exports = mongoose.model('UserRopa', UserRopaSchema);


