const mongoose = require("mongoose");
const Consumible = require("../models/Consumible");

const SkinEspecialSchema = mongoose.Schema({
  nombreSkin: {
    type: String,
    required: true,
  },
  descripcion: {
    type: String,
    required: true,
  },

  costo: {
    type: Number,
    required: true,
    default: 0,
  },
  imagenSkin: {
    type: String,
    required: true,
    trim: true,
    default: "",
  },
  danoEspecial: {
    type: Number,
    required: false,
  },
  puntosDeVidaExtra: {
    type: Number,
    required: false,
  },
  disponibilidadTiempo: {
    type: Number,
    required: true,
    default: 0,
  },
 consumiblesIncluidos:{  //ojo con esta parte, almacena arreglo de objetos
    type: [String],
    required: false,
    default:[]

 },
  objectTag: {
    type: String,
    required: true,
    default: "",
  },
  estado: {
    type: Boolean,
    default: true,
  },
});
module.exports = mongoose.model("SkinEspecial", SkinEspecialSchema);
