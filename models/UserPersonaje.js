const mongoose = require('mongoose');

const UserPersonajeSchema = mongoose.Schema(
    {
        nombrePersonaje:{
            type: String,
            required: true,
            trim: true,
        },
        imagenPersonaje:{
            type: String,
            required: true,
            trim: true,
        },
    
        objectTag:{
            type: String,
            required: true,
            trim: true,
        },
        
        idUsuario: {
            type: mongoose.Schema.Types.ObjectId,
            ref:'Usuario',
            required: true
        },
        idPersonaje: {
            type: mongoose.Schema.Types.ObjectId,
            ref:'Personaje',
            required: true
        },
       
    }
);
module.exports = mongoose.model('UserPersonaje', UserPersonajeSchema);


