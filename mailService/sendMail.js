const MailSlurp = require("mailslurp-client").default;



//funcion para enviar correo electronico de confirmacion a un correo establecido
//configuro con mi API KEY
const sendMailToUser = async (inboxPageid, clave, correo) => { //aqui ingresan el id del inbox, la clave temporal obtenida de manera aleatoria, y el correo del cliente
  //await mailslurp.createInbox();
  try {
    const mailslurp = new MailSlurp({
      apiKey: "19f12c4041b1fbad56877e5d515409395423d1545eb56e6e7dfa56566f1be4bc",
    });
    await mailslurp.sendEmail(inboxPageid, {
      to: [correo],
      subject: "Recuperación de Contraseña",
      body: `
    <article>
      <h1>Go Protest Game</h1>
      <img  src="https://picsum.photos/200/300"  width="300"
      height="250">
      <h2>Recuperación de contraseña</h2>
      <p>Esta es su clave temporal, ingrese a su cuenta con esta clave y luego proceda a cambiarla</p>
      <p>Clave temporal: <b>${clave}</b></p>
      <p>Le deseamos un buen día</p>
    </article>`,
      charset: "utf8",
      html: true,
    });
    console.log("Correo enviado con Éxito a "+ correo);
  } catch (error) {
    console.log('no funciona enviar el correo')
    console.log(error);
  }
  
 

};

module.exports = sendMailToUser;
