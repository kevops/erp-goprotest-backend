const mongoose = require('mongoose');
const ComprasSchema = new mongoose.Schema({

    nombreArticulo: {
        type: String,
        require: true,
        trim: true
    },
    IDConsumible:{
        type: mongoose.Schema.Types.ObjectId,
        ref:'Consumible',
        require: false,
    },
    IDRopa:{
        type: mongoose.Schema.Types.ObjectId,
        ref:'Ropa',
        require: false,
    },
    IDSkinEspecial:{
        type: mongoose.Schema.Types.ObjectId,
        ref:'SkinEspecial',
        require: false,
    },
    IDPersonaje:{
        type: mongoose.Schema.Types.ObjectId,
        ref:'Personaje',
        require: false,
    },
    fechaCompra: {
        type: Date,
        default: Date.now()
    },
    costo: {
        type: Number,
        required: true,
        default: 0,
      },
    IDUsuario:{
        type: mongoose.Schema.Types.ObjectId,
        ref:'Usuario',
        require: true,
    },

})
module.exports = mongoose.model('Compras', ComprasSchema);