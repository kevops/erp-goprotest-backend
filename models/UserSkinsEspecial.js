const mongoose = require('mongoose');

const UserSkinEspecialSchema = mongoose.Schema(
    {
        nombreSkin:{
            type: String,
            required: true,
            trim: true,
        },
        imagenSkin:{
            type: String,
            required: true,
            trim: true,
        },
    
        objectTag:{
            type: String,
            required: true,
            trim: true,
        },
        cantidadDisponible:{
            type: Number,
            required: true,
            trim: true
        },
        
        idUsuario: {
            type: mongoose.Schema.Types.ObjectId,
            ref:'Usuario'
        },
        idSkinEspecial: {
            type: mongoose.Schema.Types.ObjectId,
            ref:'SkinEspecial'
        },
       
    }
);
module.exports = mongoose.model('UserSkinEspecial', UserSkinEspecialSchema);


