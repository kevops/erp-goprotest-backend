const mongoose = require('mongoose');

const UserConsumibleSchema = mongoose.Schema(
    {
        nombreConsumible:{
            type: String,
            required: true,
            trim: true,
        },
        imagenConsumible:{
            type: String,
            required: true,
            trim: true,
        },
        
        cantidadDisponible:{
            type: Number,
            required:true,
            default: 0,
        },
        objectTag:{
            type: String,
            required: true,
            trim: true,
        },
        
        idUsuario: {
            type: mongoose.Schema.Types.ObjectId,
            ref:'Usuario',
            required:true
        },
        idConsumible: {
            type: mongoose.Schema.Types.ObjectId,
            ref:'Consumible',
            required:true
        },
       
    }
);
module.exports = mongoose.model('UserConsumible', UserConsumibleSchema);


