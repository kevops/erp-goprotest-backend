const mongoose = require("mongoose");

const CancionSchema = mongoose.Schema({
  nombre: {
    type: String,
    required: true,
  },
  descripcion: {
    type: String,
    required: true,
  },

  urlCancion: {
    type: String,
    required: true,
    default: "url cancion",
  },
  autor: {
    type: String,
    required: true,
    default: "",
  },
  lyrics: {
    type: String,
    required: true,
    default: "letra de la cancion",
  },
  
  objectTag: {
    type: String,
    required: true,
    default: "",
  },
  
});
module.exports = mongoose.model("Cancion", CancionSchema);
