const mongoose = require("mongoose");

const ConsumibleSchema = mongoose.Schema({
  nombre: {
    type: String,
    required: true,
  },
  tipo: {
    type: String,
    required: true,
    trim: true,
  },
  descripcion: {
    type: String,
    required: true,
  },

  costo: {
    type: Number,
    required: true,
    default: 0,
  },
  imagen: {
    type: String,
    required: true,
    trim: true,
    default: "",
  },
  dano: {
    type: Number,
    required: false,
  },
  puntosDeVida: {
    type: Number,
    required: false,
  },
  cantidad: {
    type: Number,
    required: true,
    default: 0,
  },
  danoPorSegundo: {
    type: Number,
    required: false,
    default: 0,
  },
  incrementoAliados: {
    type: Number,
    required: false,
    default: 0,
  },
  tiempoLiberacion: {
    type: Number,
    required: false,
    default: 0,
  },
  objectTag: {
    type: String,
    required: true,
    default: "",
  },
  estado: {
    type: Boolean,
    default: true,
  },
});
module.exports = mongoose.model("Consumible", ConsumibleSchema);
